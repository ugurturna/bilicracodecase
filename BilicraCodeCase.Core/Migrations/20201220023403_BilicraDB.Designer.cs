﻿// <auto-generated />
using System;
using BilicraCodeCase.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace BilicraCodeCase.Core.Migrations
{
    [DbContext(typeof(BilicraContext))]
    [Migration("20201220023403_BilicraDB")]
    partial class BilicraDB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 64)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("BilicraCodeCase.Core.Catalog", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedOn")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime(6)")
                        .HasDefaultValue(new DateTime(2020, 12, 20, 5, 34, 2, 984, DateTimeKind.Local).AddTicks(763));

                    b.Property<bool>("IsDeleted")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("tinyint(1)")
                        .HasDefaultValue(false);

                    b.Property<string>("Name")
                        .HasColumnType("varchar(255)");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Catalog");
                });

            modelBuilder.Entity("BilicraCodeCase.Core.CatalogMap", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("CatalogId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedOn")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime(6)")
                        .HasDefaultValue(new DateTime(2020, 12, 20, 5, 34, 2, 988, DateTimeKind.Local).AddTicks(7064));

                    b.Property<bool>("IsDeleted")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("tinyint(1)")
                        .HasDefaultValue(false);

                    b.Property<int>("ProductId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasAlternateKey("CatalogId", "ProductId");

                    b.HasIndex("ProductId");

                    b.ToTable("CatalogMap");
                });

            modelBuilder.Entity("BilicraCodeCase.Core.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Code")
                        .HasColumnType("varchar(255)");

                    b.Property<DateTime>("CreatedOn")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime(6)")
                        .HasDefaultValue(new DateTime(2020, 12, 20, 5, 34, 2, 983, DateTimeKind.Local).AddTicks(1708));

                    b.Property<bool>("IsDeleted")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("tinyint(1)")
                        .HasDefaultValue(false);

                    b.Property<DateTime>("LastUpdated")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Name")
                        .HasColumnType("longtext");

                    b.Property<string>("Photo")
                        .HasColumnType("longtext");

                    b.Property<decimal>("Price")
                        .HasColumnType("decimal(65,30)");

                    b.HasKey("Id");

                    b.HasIndex("Code")
                        .IsUnique();

                    b.ToTable("Product");
                });

            modelBuilder.Entity("BilicraCodeCase.Core.CatalogMap", b =>
                {
                    b.HasOne("BilicraCodeCase.Core.Catalog", "Catalog")
                        .WithMany("CatalogMap")
                        .HasForeignKey("CatalogId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("BilicraCodeCase.Core.Product", "Product")
                        .WithMany("CatalogMap")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Catalog");

                    b.Navigation("Product");
                });

            modelBuilder.Entity("BilicraCodeCase.Core.Catalog", b =>
                {
                    b.Navigation("CatalogMap");
                });

            modelBuilder.Entity("BilicraCodeCase.Core.Product", b =>
                {
                    b.Navigation("CatalogMap");
                });
#pragma warning restore 612, 618
        }
    }
}
