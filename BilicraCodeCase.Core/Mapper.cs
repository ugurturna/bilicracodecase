﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Text;

namespace BilicraCodeCase.Core
{
    public class Mapper : IMapper
    {
        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return source.Adapt<TDestination>();
        }
    }
}
