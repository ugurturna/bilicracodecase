﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BilicraCodeCase.Core
{
    public class CatalogMapInput
    {
        [Required]
        public int CatalogId { get; set; }
        [Required]
        public List<int> ProductIds { get; set; }
    }
}
