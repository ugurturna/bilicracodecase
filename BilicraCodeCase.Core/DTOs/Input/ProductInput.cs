﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BilicraCodeCase.Core
{

    public class ProductInput
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]

        [Range(0, 999, ErrorMessage = "Price 0-999 arası olmalıdır.")]
        public decimal Price { get; set; }

        public IFormFile Photo { get; set; }


    }
}
