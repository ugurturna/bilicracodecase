﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BilicraCodeCase.Core
{
    public class CatalogMapOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ProductOutput> Products { get; set; }
    }
}
