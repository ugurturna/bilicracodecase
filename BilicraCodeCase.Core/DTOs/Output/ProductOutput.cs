﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BilicraCodeCase.Core
{
    public class ProductOutput
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Photo { get; set; }
        public decimal Price { get; set; }
        public string LastUpdated { get; set; }
    }
}
