﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BilicraCodeCase.Core.Repository
{
    public class Repository<TEntity> : IRepository<TEntity>
      where TEntity : class
    {
        #region Base


        public readonly BilicraContext Context;
        private readonly DbSet<TEntity> _entities;
        public Repository(BilicraContext context)
        {
            Context = context;
            _entities = context.Set<TEntity>();
        }

        private int SaveChanges()
        {
            return Context.SaveChanges();
        }



        public TEntity Insert(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Context.Entry(entity).State = EntityState.Added;
            SaveChanges();
            return entity;
        }
        public IList<TEntity> Insert(IList<TEntity> listEntity)
        {
            if (listEntity == null)
                throw new ArgumentNullException(nameof(listEntity));

            Context.AddRange(listEntity);
            SaveChanges();
            return listEntity;
        }
        public TEntity Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _entities.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            SaveChanges();

            return entity;
        }

        public TEntity Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _entities.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            SaveChanges();
            return entity;
        }

        public IEnumerable<TEntity> GetAll() =>
               _entities.AsNoTracking().AsEnumerable();

        public TEntity GetBy(Func<TEntity, bool> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            var result = _entities.FirstOrDefault(predicate);
            return result;
        }

        public IList<TEntity> GetAllBy(Func<TEntity, bool> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            return _entities.Where(predicate).ToList();
        }

   
        #endregion

    }


}
