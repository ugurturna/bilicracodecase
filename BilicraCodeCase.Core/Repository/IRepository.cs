﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Text;

namespace BilicraCodeCase.Core.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Insert(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Delete(TEntity entity);
        IList<TEntity> Insert(IList<TEntity> listEntity);

        IEnumerable<TEntity> GetAll();
        TEntity GetBy(Func<TEntity, bool> predicate);
        IList<TEntity> GetAllBy(Func<TEntity, bool> predicate);
    }
   

}
