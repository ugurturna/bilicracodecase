﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BilicraCodeCase.Core
{
    public class BilicraContext : DbContext
    {
        public BilicraContext(DbContextOptions<BilicraContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<Product>(e =>
            {
                e.HasIndex(k => k.Code).IsUnique();
                e.HasKey(k => k.Id);
                e.Property(k => k.IsDeleted).HasDefaultValue(false);
                e.Property(k => k.CreatedOn).HasDefaultValue(DateTime.Now);

            });
            modelBuilder.Entity<Catalog>(e =>
            {
                e.HasIndex(k => k.Name).IsUnique();
                e.HasKey(k => k.Id);
                e.Property(k => k.IsDeleted).HasDefaultValue(false);
                e.Property(k => k.CreatedOn).HasDefaultValue(DateTime.Now);

            });

            modelBuilder.Entity<CatalogMap>(e =>
            {
                e.HasKey(k => k.Id);
                e.HasAlternateKey(k => new { k.CatalogId, k.ProductId });
                e.Property(k => k.IsDeleted).HasDefaultValue(false);
                e.Property(k => k.CreatedOn).HasDefaultValue(DateTime.Now);
            });
            modelBuilder.Entity<CatalogMap>()
                .HasOne(hk => hk.Catalog)
                .WithMany(h => h.CatalogMap)
                .HasForeignKey(hk => hk.CatalogId);

            modelBuilder.Entity<CatalogMap>()
              .HasOne(hk => hk.Product)
              .WithMany(h => h.CatalogMap)
              .HasForeignKey(hk => hk.ProductId);


            foreach (var type in modelBuilder.Model.GetEntityTypes())
            {
                if (typeof(IBaseEntity).IsAssignableFrom(type.ClrType) && (type.BaseType == null || !typeof(IBaseEntity).IsAssignableFrom(type.BaseType.ClrType)))
                {
                    modelBuilder.SetSoftDeleteFilter(type.ClrType);
                }
            }

            base.OnModelCreating(modelBuilder);

        }

        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Catalog> Catalog { get; set; }
        public virtual DbSet<CatalogMap> CatalogMap { get; set; }


    }
    public static class CheckIsDeleted
    {
        public static void SetSoftDeleteFilter(this ModelBuilder modelBuilder, Type entityType)
        {
            SetSoftDeleteFilterMethod.MakeGenericMethod(entityType)
                .Invoke(null, new object[] { modelBuilder });
        }

        static readonly MethodInfo SetSoftDeleteFilterMethod = typeof(CheckIsDeleted)
                   .GetMethods(BindingFlags.Public | BindingFlags.Static)
                   .Single(t => t.IsGenericMethod && t.Name == "SetSoftDeleteFilter");

        public static void SetSoftDeleteFilter<TEntity>(this ModelBuilder modelBuilder)
            where TEntity : class, IBaseEntity
        {
            modelBuilder.Entity<TEntity>().HasQueryFilter(x => !x.IsDeleted);
        }
    }
}
