﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BilicraCodeCase.Core
{
    public class Product : BaseEntity
    {

        public string Code { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public decimal Price { get; set; }
        public DateTime LastUpdated { get; set; } = DateTime.Now;

        public virtual ICollection<CatalogMap> CatalogMap { get; set; }
    }
}
