﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BilicraCodeCase.Core
{
    public class CatalogMap : BaseEntity
    {
     
        public int CatalogId { get; set; }
        public virtual Catalog Catalog { get; set; }
    
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }

    }
}
