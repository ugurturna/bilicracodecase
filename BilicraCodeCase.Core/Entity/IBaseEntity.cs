﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BilicraCodeCase.Core
{
   public partial interface IBaseEntity
    {
        bool IsDeleted { get; set; }
    }
}
