﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BilicraCodeCase.Core
{
    public class Catalog : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<CatalogMap> CatalogMap { get; set; }
    }
}
