﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BilicraCodeCase.Core
{
   public interface IMapper
    {
        TDestination Map<TSource, TDestination>(TSource source);
    }
}
