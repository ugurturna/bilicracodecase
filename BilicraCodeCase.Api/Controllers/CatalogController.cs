﻿using BilicraCodeCase.Core;
using BilicraCodeCase.Core.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BilicraCodeCase.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CatalogController : ControllerBase
    {
        private readonly IRepository<Catalog> _catalogRepository;
        private readonly IRepository<CatalogMap> _catalogMapRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IMapper _mapper;

        public CatalogController(IRepository<Catalog> catalogRepository,
            IMapper mapper,
            IRepository<CatalogMap> catalogMapRepository,
            IRepository<Product> productRepository)
        {
            _catalogRepository = catalogRepository;
            _mapper = mapper;
            _catalogMapRepository = catalogMapRepository;
            _productRepository = productRepository;
        }

        private CatalogMapOutput PrepareCatalog(List<CatalogMap> map)
        {
            var catalog = _catalogRepository.GetBy(x => x.Id.Equals(map.FirstOrDefault().CatalogId));
            var productIds = map.Select(y => y.ProductId).ToList();
            var products = _productRepository.GetAll().Where(x => productIds.Contains(x.Id));

            var result = _mapper.Map<Catalog, CatalogMapOutput>(catalog);
            result.Products = new List<ProductOutput>();
            foreach (var item in products)
            {
                ProductOutput product = _mapper.Map<Product, ProductOutput>(item);
                result.Products.Add(product);
            }
            return result;
        }

        /// <summary>
        /// Catalog Bilgisi Ekleme
        /// </summary>
        /// <param name="catalogName"></param>
        /// <returns></returns>
        [HttpPut("Insert")]
        public IActionResult InsertCatalog([FromForm] string catalogName)
        {
            var result = _catalogRepository.Insert(new Catalog() { Name = catalogName });
            return Ok(new ApiResponse(true, 200, "Kayıt Başarılı", result.Id, result));
        }

        /// <summary>
        /// Tanımlanmış kataloglara ürün ekleme
        /// </summary>
        /// <param name="catalogMap"></param>
        /// <returns></returns>
        [HttpPut("InsertContent")]
        public IActionResult InsertCatalogContent([FromForm] CatalogMapInput catalogMap)
        {
            var catalogMapEntity = new List<CatalogMap>();

            foreach (var productId in catalogMap.ProductIds)
            {
                if (_productRepository.GetAll().Any(x => x.Id.Equals(productId)))
                {
                    catalogMapEntity.Add(new CatalogMap() { CatalogId = catalogMap.CatalogId, ProductId = productId });
                }
            }

            _catalogMapRepository.Insert(catalogMapEntity);
            return Ok(new ApiResponse(true, 200, "Kayıt Başarılı", 0, catalogMapEntity));
        }

        /// <summary>
        /// Catalog içerisindeki Productların listelenmesi.
        /// </summary>
        /// <param name="catalogId"></param>
        /// <returns></returns>
        [HttpGet("GetCatalogContent/{catalogId}")]
        public IActionResult GetCatalogContent(int catalogId)
        {
            try
            {

                var catalogMap = _catalogMapRepository.GetAll().Where(x => x.CatalogId.Equals(catalogId)).ToList();

                var result = PrepareCatalog(catalogMap);

                return Ok(new ApiResponse(true, 200, "Başarılı", 0, result));
            }
            catch (System.Exception ex)
            {
                return BadRequest(new ApiResponse(false, 400, ex.Message, 0, ex));
            }
        }


        /// <summary>
        /// Catalog içerisindeki productların excel'e çıkarılması.
        /// </summary>
        /// <param name="catalogId"></param>
        /// <returns></returns>
        [HttpGet("ExportCatalogContent/{catalogId}")]
        public IActionResult ExportToExcel(int catalogId)
        {
            var productIds = _catalogMapRepository.GetAllBy(x => x.CatalogId.Equals(catalogId)).Select(y => y.ProductId).ToList();
            var products = _productRepository.GetAll().Where(x => productIds.Contains(x.Id)).ToList();
            return File(
                         Helper.ExportToExcel<Product>(products),
                         "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                         $"Products-{DateTime.Now}.xlsx");
        }


    }
}
