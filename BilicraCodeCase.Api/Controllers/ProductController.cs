﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BilicraCodeCase.Core;
using BilicraCodeCase.Core.Repository;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BilicraCodeCase.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]


    public class ProductController : ControllerBase
    {
        private readonly IRepository<Product> _productRepository;
        private readonly IMapper _mapper;
        public static IWebHostEnvironment _environment;

        public ProductController(IRepository<Product> productRepository,
            IMapper mapper,
            IWebHostEnvironment environment)
        {
            _productRepository = productRepository;
            _mapper = mapper;
            _environment = environment;
        }

        #region util

        private string UploadPhoto(IFormFile photo)
        {
            if (photo == null)
                return string.Empty;

            string fileName = $"Photos/{photo.FileName}";
            string path = Path.Combine(_environment.ContentRootPath, fileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                photo.CopyTo(stream);
            }
            return fileName;
        }
        #endregion

        /// <summary>
        /// Product bilgilerinin getirilmesi
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet("Get/{productId}")]
        public IActionResult GetProduct(int productId)
        {
            try
            {
                var product = _productRepository.GetBy(x => x.Id.Equals(productId));
                if (product != null)
                    return Ok(new ApiResponse(true, 200, "Başarılı", product.Id, _mapper.Map<Product, ProductOutput>(product)));
                else
                    return BadRequest(new ApiResponse(false, 404, "Ürün Bulunumadı.", productId, null));

            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse(false, 500, ex.Message, productId, ex));
            }
        }
        /// <summary>
        /// Sistemde kayıtlı olan bütün productların listelenmesi
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAll")]
        public IActionResult GetAllProducts()
        {
            try
            {
                return Ok(new ApiResponse(true, 200, "Başarılı", 0, _productRepository.GetAll()));

            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse(false, 500, ex.Message, 0, ex));
            }
        }

        /// <summary>
        /// Product Ekleme
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPut("Insert")]
        public IActionResult InsertProduct([FromForm] ProductInput product)
        {
            if (ModelState.IsValid && !_productRepository.GetAll().Any(x => x.Code.Equals(product.Code)))
            {
                Product productEntity = _mapper.Map<ProductInput, Product>(product);
                productEntity.Photo = UploadPhoto(product.Photo);
                var saved = _productRepository.Insert(productEntity);

                return Ok(new ApiResponse(true, 200, "Kayıt Başarılı", saved.Id, saved));
            }
            else
            {
                if (ModelState.ErrorCount.Equals(0))
                    ModelState.AddModelError("Code Unique", "Code benzersiz olmalıdır.");

                return BadRequest(new ApiResponse(false, 400, "Hata", 0, data: ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList()));
            }

        }

        /// <summary>
        /// Product Soft Delete
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpDelete("Delete")]
        public IActionResult DeleteProduct([FromForm] int productId)
        {
            try
            {
                var entity = _productRepository.GetBy(x => x.Id.Equals(productId));
                entity.IsDeleted = true;
                _productRepository.Update(entity);
                return Ok(new ApiResponse(true, 200, "Kayıt Silindi", productId, entity));
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse(false, 500, ex.Message, productId, ex));
            }
        }

        /// <summary>
        /// Product Güncellenmesi. Gönderilen veriler olduğu gibi update edilir.
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost("Edit")]
        public IActionResult UpdateProduct([FromForm] ProductInputId product)
        {
            try
            {
                Product productEntity = _mapper.Map<ProductInputId, Product>(product);
                productEntity.LastUpdated = DateTime.Now;
                _productRepository.Update(productEntity);
                return Ok(new ApiResponse(true, 200, "Kayıt Güncellendi.", product.Id, productEntity));
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse(false, 500, ex.Message, product.Id, ex));
            }
        }

        /// <summary>
        /// Sistemdeki Bütün Productların Excel'e export edilmesi.
        /// </summary>
        /// <returns></returns>
        [HttpGet("ExportAllProduct")]
        public IActionResult ExportToExcel()
        {
            return File(
                         Helper.ExportToExcel<Product>(_productRepository.GetAll().ToList()),
                         "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                         $"Products-{DateTime.Now}.xlsx");
        }
    }
}
