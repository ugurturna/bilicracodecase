﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BilicraCodeCase
{
    public static class Helper
    {
        public static byte[] ExportToExcel<TEntity>(List<TEntity> entity)
        {
            using (var workbook = new XLWorkbook())
            {
                var entityType = entity.First().GetType();
                var worksheet = workbook.Worksheets.Add();
                int currentRow = 1, i = 0;

                foreach (var item in entityType.GetProperties())
                {
                    ++i;
                    worksheet.Cell(currentRow, i).Value = item.Name;
                }
                foreach (var entityItem in entity)
                {
                    currentRow++;
                    int j = 0;
                    foreach (var item in entityItem.GetType().GetProperties())
                    {
                        ++j;
                        worksheet.Cell(currentRow, j).Value = item.GetValue(entityItem);
                    }

                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return content;
                      
                }
            }
        }
    }
}
